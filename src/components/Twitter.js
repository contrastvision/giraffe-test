import { useEffect, useState } from "react";
import fetchUser from "../service/fetchUser";

const Twitter = (props) => {
    const { username, children } = props;
    const [user, setUser] = useState(null);
    useEffect(() => {
        fetchUser(username)
            .then((res) => {
                setUser(res.data);
            })
            .catch(function (e) {
                console.log(e);
            });
    }, [username]);

    return children(user);
};

export default Twitter;
