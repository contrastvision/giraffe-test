const Badge = ({ info }) => {
    return (
        <div className="card mx-auto" style={{width: "18rem"}}>
            <div className="card-body">
                <img src={info.avatarUrl} className="img-thumbnail mb-3" alt={info.name}/>
                <h5 className="card-title">{info.name}</h5>
                <p className="card-text">Age: {info.age}</p>
            </div>
        </div>
    );
};

export default Badge;
