const Loader = () => {
    return (
        <div className="spinner-border"></div>
    )
}

export default Loader;