import { useEffect, useState } from "react";

const SORT_TYPES = {
    title: "title",
    price: "price",
    count: "count",
};

const Products = () => {
    const [products, setProducts] = useState([]);
    const [sortType, setSortType] = useState("title");

    const getProducts = () => {
        return [
            {
                id: 1,
                title: "Good tea",
                price: 10,
                count: 13,
            },
            {
                id: 2,
                title: "Good coffee",
                price: 15,
                count: 31,
            },
        ];
    };

    useEffect(() => {
        const sortArray = (type) => {
            const sortBy = SORT_TYPES[type];
            const items = getProducts();
            const sorted = [...items].sort((a, b) => {
                if (a[sortBy] < b[sortBy]) {
                    return -1;
                }
                if (a[sortBy] > b[sortBy]) {
                    return 1;
                }
                return 0;
            });

            setProducts(sorted);
        };
        sortArray(sortType);
    }, [sortType]);

    return (
        <div className="products ">
            <select className="form-select products-sort mb-4" onChange={(e) => setSortType(e.target.value)}>
                <option value="title">Name</option>
                <option value="price">Price</option>
                <option value="count">Count</option>
            </select>
            <div className="products-list row row-cols-1 row-cols-md-2 mb-3 text-center">
                {products.map((product) => (
                    <div key={product.id} className="col">
                        <div className="card mb-4 rounded-3 shadow-sm">
                            <div className="card-body">
                                <h1 className="card-title pricing-card-title mb-3">{product.title}</h1>
                                <div>Price: {product.price}$</div>
                                <div>Quantity: {product.count}</div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Products;
