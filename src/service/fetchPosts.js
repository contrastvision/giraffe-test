const fetchPosts = () => {
    return fetch("https://jsonplaceholder.typicode.com/posts?_limit=2")
        .then((response) => {
            if (!response.ok) {
                throw new Error(response.status);
            } else {
                return response.json();
            }
        })
        .then((result) => {
            return result;
        })
        .catch((e) => {
            throw new Error(`Request failed! Reason: ${e.message}`);
        });
};

const fetchPostsAsyncAwait = async () => {
    try {
        const response = await fetch("https://jsonplaceholder.typicode.com/posts?_limit=2");
        if (!response.ok) {
            throw new Error(response.status);
        } else {
            const result = await response.json();
            return result;
        }
    } catch (e) {
        throw new Error(`Request failed! Reason: ${e.message}`);
    }
};

fetchPosts()
    .then((data) => {
        console.log(data);
    })
    .catch((e) => console.log(e.message));

fetchPostsAsyncAwait()
    .then((data) => {
        console.log(data);
    })
    .catch((e) => console.log(e.message));
