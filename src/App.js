import Badge from "./components/Badge";
import Loader from "./components/Loader";
import Products from "./components/Products";
import Twitter from "./components/Twitter";

function App() {
    return (
        <div className="App">
            <div className="container py-3">
                <main>
                    <div>
                        <h1>Task 1</h1>
                        <Products />
                    </div>
                    <div>
                        <h1>Task 2</h1>
                        <Twitter username="tylermcginnis33">
                            {(user) => (user === null ? <Loader /> : <Badge info={user} />)}
                        </Twitter>
                    </div>
                    <div>
                        <h1>Task 3</h1>
                        <p>In file /service/fetchPosts.js</p>
                    </div>
                </main>
            </div>
        </div>
    );
}

export default App;
